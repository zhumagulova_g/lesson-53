import React, {useState} from 'react';
import './App.css';
import AddTaskForm from './Components/AddTaskForm';
import Task from './Components/Task';
import {nanoid} from 'nanoid'

const App = () => {

  const [task, setTask] = useState([
    {text: "To go to market", id: "1"},
    {text: "to buy bread", id: "2"},
    {text: "to buy some apple", id: "3"},
  ]);

  const [currentTask, setCurrentTask] = useState("");

  const changeText = event => {
    const currentVal = event.target.value;
    setCurrentTask(currentVal);
  };

  const addTask = () => {
    const textCopy = {};
    textCopy.text = currentTask;
    textCopy.id = nanoid();

    const taskCopy = [...task];
    taskCopy.push(textCopy);
    setTask(taskCopy);
  };

  const removeTask = id => {
      const index = task.findIndex(text => text.id === id);
      const taskCopy = [...task];
      taskCopy.splice(index, 1);
      setTask(taskCopy)
  };

  return (
    <div className="App">
        <div>
            <AddTaskForm onChangeText = {changeText}
                         onAddClick = {addTask}>
            </AddTaskForm>
        </div>
        <div>
            {task.map((task, index) => {
              return <Task
                  key = {index}
                  text = {task.text}
                  remove = {() => {removeTask(task.id)}}>
              </Task>
            })}
        </div>
    </div>
  );
};

export default App;
