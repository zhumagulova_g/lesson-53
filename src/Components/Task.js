import React from 'react';

const Task = props => {
    return (
        <div className="task">

            <div className="task-class">
                <input type="checkbox" className="box-class" name = "box"/>
                {props.text}
                <button onClick={props.remove}>Remove</button>
            </div>

        </div>
    )
};

export default Task;