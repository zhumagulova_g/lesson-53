import React from 'react';

const AddTaskForm = props => {
    return (
        <div>
            <input className="add-class" type="text"
                   value={props.currentTask} onChange={props.onChangeText}/>
            <button onClick={props.onAddClick}>Add</button>
        </div>
    )
};

export default AddTaskForm;